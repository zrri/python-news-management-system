## 新闻管理系统

- 创建数据库

```sql
CREATE DATABASE vega;
USE vega;
CREATE TABLE t_type(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	type VARCHAR(20) NOT NULL UNIQUE
 ); 
INSERT INTO t_type(type) VALUES("要闻"),("体育"),("科技"),("娱乐"),("历史");


CREATE TABLE t_role(
 id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
 role VARCHAR(20) NOT NULL UNIQUE
);
INSERT INTO t_role(role) VALUES("管理员"),("新闻编辑");


SELECT AES_ENCRYPT("123456","ZRR123"); # 加密
SELECT HEX(AES_ENCRYPT("123456","ZRR123")); # 16进制
SELECT AES_DECRYPT(
UNHEX("C5E48EF4B57987C8AAB83228571FAF5D"),"ZRR123"); # 解密  


CREATE TABLE t_user(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	username VARCHAR(20) NOT NULL UNIQUE,
	password VARCHAR(500) NOT NULL,
	email VARCHAR(100) NOT NULL,
	role_id INT UNSIGNED NOT NULL,
	INDEX(username) 
);
INSERT INTO t_user(username,password,email,role_id)
VALUES("admin",HEX(AES_ENCRYPT("123456","ZRR123")),"admin@163.com",1);
INSERT INTO t_user(username,password,email,role_id)
VALUES("scott",HEX(AES_ENCRYPT("123456","ZRR123")),"admin@163.com",2);

CREATE TABLE t_news(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	title VARCHAR(40) NOT NULL,
	editor_id INT UNSIGNED NOT NULL,
	type_id INT UNSIGNED NOT NULL,
	content_id CHAR(12) NOT NULL,
	is_top TINYINT UNSIGNED NOT NULL,
	create_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	update_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	state ENUM("草稿","待审批","已审批","隐藏") NOT NULL,
	INDEX(editor_id),
	INDEX(type_id),
	INDEX(state),
	INDEX(create_time),
	INDEX(is_top)
);
```

## 更新pip

![image-20210216170848805](https://gitee.com/zrri/img/raw/master/img/image-20210216170848805.png)

## 安装Colorama

![image-20210216170913584](https://gitee.com/zrri/img/raw/master/img/image-20210216170913584.png)

## 项目功能

- `vgea`
  - `db`
    - [x] mysql_db.py
    - [x] news_dao.py
    - [x] role_dao.py
    - [x] user_dao.py
  - `service`
    - [x] news_service.py
    - [x] role_service.py
    - [x] user_service.py
  - `app.py`

## 功能

| 系统登录 退出系统    |
| -------------------- |
| 新闻管理 用户管理    |
| 审批新闻 删除新闻    |
| 添加，修改，删除用户 |

## 界面

![image-20210216172009366](https://gitee.com/zrri/img/raw/master/img/image-20210216172009366.png)
